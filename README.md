# Firmware for Seapoint turbidity sensor interface board

This repo contains the firmware for the Raven ROV interface board for the [Seapoint Sensors](http://www.seapoint.com/) [Turbidity sensor](http://www.seapoint.com/stm.htm). The Kicad hardware files are stored in  the [turbidity_sensor_board](https://gitlab.com/amarburg/turbidity_sensor_board) repo.

The Seapoint turbidity sensor has a 0-5V analog output.  This output is proportional to the sensed turbidity, with four selectable gains (25, 125, 500, and 4000 NTU).

![Block diagram](imgs/block_diagram.svg)

The turbidity sensor interface board is a simple analog-to-digital-to-serial converter.   It is built around an [Adafruit Trinket M0](https://www.adafruit.com/product/3500) development board and uses its internal 10-bit ADC to sample the Seapoint.  The Trinket is programmed in [CircuitPython](https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino/circuitpython) and has two user-facing interfaces:

 * The Trinket micro-USB port includes both a filesystem interface, which is used to reprogram the Trinket, and a virtual serial port which provides turbidity data (see below).
 *  The turbidity data interface is mirrored on the Trinket's hardware UART.  This serial interface is at 3.3V TTL levels.

# Using this firmware

The standard Circuitpython bootloader offers a simple programming interface.  When plugged into USB, the Trinket will appear as a USB drive.  Copy the `main.py` in this repo to the root of the drive and the Trinket will automatically reboot and start running the new Python script.  This codes does not require any specialized libraries in the `lib/` directory on the Trinket.

# Interface

The interface polls the circuit board once per second.  On each cycle it samples the sensor 10 times over 100ms and uses the average value.  This sample is in "ADC counts".  These counts are converted to Seapoint output voltage, and then to NTUs.

The sensor produces a pseudo-NMEA output string on both the USB virtual serial port and on the real hardware serial port:

    $TRBSP,<counts>,<volts>,<ntu>,<range>\r\n

for example:

    $TRBSP,9238,0.68,3,25\r\n

Where:

 * `counts` is the raw ADC counts (0-65535), 
 * `volts` is the equivalent voltage from the Seapoint (0-5V),
 * `ntu` is the NTU given the current Seapoint range, and 
 * `range` is the current Seapoint gain setting, given as the max NTU value (which occurs at 5V output)

So in this example, the raw ADC value of 9238 is equivalent to 0.68V from the Seapoint, or 3 NTU at the current gain setting of 0-25 NTUs.

The gain setting of the unit can be configured by sending the following to the interface board:

     $TRBSP,MODE,<range>\r\n

Where `range` is 25,125,500, or 4000.  As noted in the [Seapoint Manual](http://www.seapoint.com/pdf/stm_um.pdf), the first three modes are linear, while the unit is only linear up to ~1250 NTU in the fourth mode.

**In the current version of the firmware, the range mode setting only works over the virtual USB port.  This will be resolved soon.**

# License

This code is released under the [BSD 3-clause license.](LICENSE)
