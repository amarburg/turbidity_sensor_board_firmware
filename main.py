# Code for Trinket M0 board to support the "Seapoint turbidity sensor board"
#
#        This code: https://github.com/amarburg/turbidity_sensor_board_firmware
#   Hardware files: https://github.com/amarburg/turbidity_sensor_board
#

import board
from digitalio import DigitalInOut, Direction, Pull
from analogio import AnalogIn
import time
import busio
import supervisor

# Set both gain outputs to high (gain on Seapoint _not_ pulled down;
#  it's pulled up by internal pullup
#
# GainA | B | Gain | Sensitivity | Range
#    1  | 1 | 100x | 200mV/FTU   | 25 FTU
#    1  | 0 |  20x |  40mV/FTU   | 125 FTU
#    0  | 1 |   5x |  10mV/FTU   | 500 FTU
#    0  | 0 |   1x |   2mV/FTU   | 4000 FTU
#
# gainAOutput control FETs (high = pulls GainX to ground), 
# so the relationship above is inverted

# sensitivity is set as the inverse of above .. NTU / volt

modes = {
  25:  { "gain_select": [False,False], "sensitivity": 5 },
  125: { "gain_select": [False,True], "sensitivity": 25 },
  500: { "gain_select": [True,False], "sensitivity": 100 },
  4000: { "gain_select": [True,True], "sensitivity": 500 }
}


### Hardware setup

# Built in red LED
#led = DigitalInOut(board.D13)
#led.direction = Direction.OUTPUT

# Analog input on D0
analog1in = AnalogIn(board.D0)

gainAout = DigitalInOut(board.D1)
gainAout.direction = Direction.OUTPUT

gainBout = DigitalInOut(board.D2)
gainBout.direction = Direction.OUTPUT

# And HW serial port
uart = busio.UART(board.TX, board.RX, baudrate=9600)

current_mode = 25

#== ==

def set_gain( mode ):
  if mode in modes:
    gainAout.value = modes[mode]["gain_select"][0]
    gainBout.value = modes[mode]["gain_select"][1]


set_gain(current_mode)


######################### HELPERS ##############################

def process_input(input):
  global current_mode
  
  tokens = input.split(',')
  if len(tokens) != 3: return

  if tokens[0] is not "$TRBSP": 
    print("!! Bad preamble")
    return
  
  if tokens[1] is "MODE":

    for key in modes:
      if int(tokens[2]) == key:
        current_mode = key
        print("!! Set mode %d" % current_mode)

        set_gain(current_mode)
        return
  
    print("!! Did not recognize mode")

  else:
     print("!! Bad command")

######################### MAIN LOOP ##############################

i = 0
while True:

  counts = []
  for i in range(10):
    counts.append( analog1in.value )
    time.sleep(0.01)

  avg_counts = sum(counts) / len(counts)

  ## Constant is the combined "theoretical" multiplier for this
  # hardware:
  #     alpha = 1/65536 * 3.3 * 1.47
  # 
  # This constant based on a very coarse calibration
  # ADC seems to max out at 65520, which corresponds to 4.95V
  # from seapoint?
  volts = avg_counts * 0.00007388888
  ntu = volts * modes[current_mode]["sensitivity"]

  # Read analog voltage on D0
  outstr = "$TRBSP,%d,%0.2f,%.0f,%d" % (avg_counts,volts,ntu,current_mode)

  print(outstr)
  uart.write( bytes(outstr + "\r\n", "utf-8") )

  ## Check for input
  if supervisor.runtime.serial_bytes_available:

    value = input().strip()
    # Sometimes Windows sends an extra (or missing) newline - ignore them
    if value == "":
       continue

    process_input( value )

  time.sleep(1.0) # make bigger to slow down